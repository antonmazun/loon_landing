<?php //var_dump($results['photo']['results']); die;?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Metropolia</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <link rel="stylesheet" href="./dist/client.min.css"/>
</head>

<body>
<header id="header">

    <div class="container">
        <div class="nav">
            <a href="#header"  data-item="mainPage" class="logo js-nav-item"></a>
            <div class="nav-list">
                <a class="js-nav-item nav-link" data-item="choice_flat" href="#">Вибрати квартиру</a>
                <a class="js-nav-item nav-link" data-item="lun" href="#">Інфраструктура</a>
                <a class="js-nav-item nav-link" data-item="price" href="#prise">Ціни</a>
                <a class="js-nav-item nav-link" data-item="about" href="#">Про нас</a>
                <a class="js-nav-item nav-link " data-item="contacts" href="#">Контакти</a>
            </div>
            <a href="#" class="action">АКЦІЯ</a>
        </div>
        <img src="front-end/img/logoCenter.png" alt="logo" id="logocenter">
        <div class="heder__contacts__block">
            <p class="coll"><?php echo $results['contact']->phone?></p>
            <p class="coll__street"><?php echo $results['contact']->address?></p>
            <p class="coll__mail"><?php echo $results['contact']->email ?></p>
        </div>

    </div>



</header>
<div class="popup-wrap" data-action="mainPage"></div>



<div id="modals">
    <div class="popup-wrap" data-action="price">
        <div class="popup-content">
            <div class="content-wrap ">
                <button class="close"></button>
                <div class="table-head padding-rule">
                    <p class="col grey-text">
                        Цены
                    </p>
                    <p class="col blue-text">
                        100% оплата
                    </p>
                    <p class="col light-grey-text">
                        Рассрочка на 22 месяца под 0%
                    </p>
                </div>
                <div class="under-head-row padding-rule">
                    <div class="col">Количество комнат</div>
                    <div class="col">м <sup>2</sup></div>
                    <div class="col">Цена</div>
                </div>
                <div class="row-wrap">
                    <?php foreach($results['appartment-price']['results'] as $appartment) { ?>
                        <div class="row-item ">
                            <div class="col"><?php echo $appartment->type; ?></div>
                            <div class="col"><?php echo $appartment->square; ?></div>
                            <div class="col"><?php echo $appartment->price_all; ?></div>
                        </div>
                    <?php }?>
                </div>
            </div>
        </div>
    </div>
    <div class="popup-wrap" data-action="lun">
        <div class="popup-content">
            <div class="lunMap" >
            </div></div></div>
    <div class="popup-wrap" data-action="about">
        <div class="popup-content">
            <div class="about-wrapper">
                <button class="close"></button>
                <h3 class="padding-rule"><?php echo $results['about-us']->type; ?></h3>
                <div class="aboutUs_content ">
                    <?php echo $results['about-us']->text; ?>
                </div>
            </div>
        </div>
    </div>

    <div class="popup-wrap" data-action="contacts">
        <div class="popup-content">
            <div id="contacts" class="contacts-wrapper">
                <button class="close"></button>

                <div class="up-block">
                    <div class="left-col">
                        <h3>Контакты</h3>
                        <p class="phone"><?php echo $results['contact']->phone?></p>
                        <p class="address"><?php echo $results['contact']->address?></p>
                        <a href="mailto:<?php echo $results['contact']->email ?>" target="_blank" class="mail"><?php echo $results['contact']->email ?></a>
                    </div>
                    <div class="right-col">
                        <img src="front-end/img/logo.png" alt="logo" class="contacts_logo">
                    </div>

                </div>
                <div class="location">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d20333.67131531907!2d30.63684555619142!3d50.42789479077554!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40d4c517ef5775d3%3A0xe05e79e4ced3ec2b!2z0YPQuy4g0KHQvtGA0LzQvtCy0YHQutCw0Y8sIDMsINCa0LjQtdCyLCAwMjAwMA!5e0!3m2!1sru!2sua!4v1551089971690"
                            width="100%" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>

    <div class="popup-wrap slider-popup" data-action="choice_flat">
        <div class="popup-content">
            <button class="close"></button>
            <div class="slider-wrapper">
                <div class="count-room-wrap">
                    <div class="count-room-item">
                        <p class="js-count-room active-flat" data-count-room="room-1">
                            Квартири
                            <br>
                            <b>однокімнатні</b>
                        </p>
                    </div>
                    <?php if(key_exists('2', $results['photo'])){?>
                        <div class=" count-room-item">
                            <p class="js-count-room " data-count-room="room-2">
                                Квартири
                                <br>
                                <b>двокімнатні</b>
                            </p>
                        </div>
                    <?php }?>
                    <?php if(key_exists('3', $results['photo'])){?>
                        <div class="count-room-item">
                            <p class="js-count-room " data-count-room="room-3">
                                Квартири
                                <br>
                                <b>трьокімнатні</b>
                            </p>
                        </div>
                    <?php }?>
                </div>
                <div class="slider-horizontal">

                    <?php foreach($results['photo'] as $key => $value){ ?>
                        <div class="js-slider slider-wrapper-content <?php echo $key != 1 ? 'hidden' : ''; ?>" data-count="room-<?php echo $key?>">
                            <div class="center room-<?php echo $key; ?>">
                                <?php if(count($value) >3) { ?>
                                    <?php foreach($value as $photo) { ?>
                                        <div class="center-item">
                                            <img src="./<?php echo $photo->filename?>" alt="">
                                            <p class="slider-text">
                                                <span class="type-flat">Тип <?php echo $photo->type?></span>
                                                <br>
                                                <b><?php echo $photo->square ?> м <sup>2</sup></b>
                                            </p>
                                        </div>
                                    <?php }?>
                                <?php } else {?>
                                    <?php
                                    if(count($value) == 1) $count = 4;
                                    if(count($value) == 2 || count($value) == 3) $count = 2;
                                    ?>
                                    <?php for($i=0;$i<$count;$i++) { ?>
                                        <?php foreach($value as $photo) { ?>
                                            <div class="center-item">
                                                <img src="./<?php echo $photo->filename?>" alt="">
                                                <p class="slider-text">
                                                    <span class="type-flat">Тип <?php echo $photo->type?></span>
                                                    <br>
                                                    <b><?php echo $photo->square ?> м <sup>2</sup></b>
                                                </p>
                                            </div>
                                        <?php }?>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                            <button class="control-btn btn-prev js-<?php echo $key?> "></button>
                            <button class="control-btn btn-next js-<?php echo $key?> "></button>
                        </div>
                    <?php }?>

                </div>
            </div>
            <div class="type-flat-wrap">
                <p class="active">Тип 1</p>
                <p>Тип 2</p>
                <p>Тип 3</p>
                <p>Тип 4</p>
                <p>Тип 5</p>
                <p>Тип 6</p>
                <p>Тип 7</p>
                <p>Тип 8</p>
                <p>Тип 9</p>
                <p>Тип 10</p>
            </div>

        </div>
    </div>
</div>

<script type="text/javascript"
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCM-U9pYJHGw9a8ySZZCz2iF4nd4vra2LE"></script>
<script src="./dist/client.bundle.js"></script>

</body>

</html>
