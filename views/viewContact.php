<?php include "templates/include/header.php" ?>
<h1 style="width: 75%;"><?php echo htmlspecialchars( $results['article']->phone )?></h1>
<div style="width: 75%; font-style: italic;"><?php echo htmlspecialchars( $results['article']->address )?></div>
<div style="width: 75%;"><?php echo $results['article']->content?></div>
<p class="pubDate">Published on <?php echo $results['article']->email?></p>
<p><a href="./">Return to Homepage</a></p>
<?php include "templates/include/footer.php" ?>
