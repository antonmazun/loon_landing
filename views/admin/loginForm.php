<?php include "views/include/header.php" ?>
<div class="row center-form">
    <form class="col s12" action="admin.php?action=login" method="post">
        <input  type="hidden" name="login" value="true"/>
        <?php if (isset($results['errorMessage'])) { ?>
            <div class="errorMessage"><?php echo $results['errorMessage'] ?></div>
        <?php } ?>
        <div class="row">
            <div class="input-field col s12">
                <input type="text" name="username" id="username" placeholder="Username" required autofocus
                       maxlength="20"/>
            </div>
            <div class="input-field col s12">
                <input type="password" name="password" id="password" placeholder="Password" required
                       maxlength="20"/>
            </div>
        </div>
        <div class="buttons">
            <button class="btn waves-effect waves-light" type="submit" name="action">Войти</button>
        </div>
    </form>
</div>

<?php //include "views/include/footer.php" ?>
