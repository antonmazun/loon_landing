
<?php include "views/include/header.php" ?>
    <div id="adminHeader">
        <p>You are logged in as <b><?php echo htmlspecialchars( $_SESSION['username']) ?></b>. <a href="admin.php?action=logout"?>Log out</a></p>
    </div>
    <h1 class="title-form"><?= $results['pageTitle']?>(<?php echo $results['totalRows']?>)</h1>
<?php if ( isset( $results['errorMessage'] ) ) { ?>
    <div class="errorMessage"><?php echo $results['errorMessage'] ?></div>
<?php } ?>
<?php if ( isset( $results['statusMessage'] ) ) { ?>
    <div class="statusMessage"><?php echo $results['statusMessage'] ?></div>
<?php } ?>
    <style>
        .row{
            display: inline-block;
            width:250px;
        }
        .row-s{
            display: inline-block;
            width:100px;
        }
    </style>

    <div class="container">
       <div class="btn-wrap">
        <a  class="btn waves-effect" href="admin.php?action=photo-create" >Додати фото</a>
        <a class="btn waves-effect waves-light red lighten-2" href="admin.php" >Назад</a>
    </div>
        <div class="table-item header-table">
            <div class="row">ID</div>
            <div class="row">К-сть кімнат</div>
            <div class="row">Площа квартири</div>
            <div class="row">Тип</div>
            <div class="row">Фото</div>
            <div class="row-s"></div>
        </div>
        <?php foreach($results['photo'] as $photo){?>
            <div class="table-item">
                <div class="row"><?= $photo->id?></div>
                <div class="row"><?= $photo->room?></div>
                <div class="row"><?= $photo->square?></div>
                <div class="row"><?= $photo->type?></div>
                <div class="row"><img width='100' src="<?= $photo->filename?>"></div>
                <div class="row-s"><a href="admin.php?action=photo-delete&id=<?=$photo->id?>">Видалити</a></div>
            </div>
        <?php } ?>
    </div>

    <!--<p><a href="admin.php?action=newArticle">Add a New Article</a></p>-->
<!--<?php include "views/include/footer.php" ?>-->