<?php include "views/include/header.php" ?>


    <div id="adminHeader">
        <p>You are logged in as <b><?php echo htmlspecialchars( $_SESSION['username']) ?></b>. <a href="admin.php?action=logout"?>Log out</a></p>
    </div>
 <h1 class="title-form"><?php echo $results['pageTitle']?></h1>
    <div class="container">
    <form action="admin.php?action=<?php echo $results['formAction']?>" method="post">
     <input class="btn waves-effect waves-light red lighten-2" type="submit" formnovalidate name="cancel" value="Назад" />
        <?php if ( isset( $results['errorMessage'] ) ) { ?>
            <div class="errorMessage"><?php echo $results['errorMessage'] ?></div>
        <?php } ?>
        <ul>
            <li>
                <label for="type">Заголовок</label>
                <input type="text" name="type" id="type" placeholder="Name of the article" required autofocus maxlength="255" value="<?php echo htmlspecialchars( $results['statpage']->type )?>" />
            </li>
            <li>
                <label for="text">Текст</label>
                <input name="text" id="text" placeholder="Brief description of the article" required maxlength="255" value="<?php echo htmlspecialchars( $results['statpage']->text )?>" >
            </li>
        </ul>
        <div class="buttons">
            <input class="btn waves-effect" type="submit" name="saveChanges" value="Зберегти" />

        </div>
    </form>
       </div>
<?php include "views/include/footer.php" ?>