<?php include "views/include/header.php" ?>
    <div id="adminHeader">
        <p>You are logged in as <b><?php echo htmlspecialchars( $_SESSION['username']) ?></b>. <a href="admin.php?action=logout"?>Log out</a></p>
    </div>
    <h1 class="title-form"><?php echo $results['pageTitle']?></h1>
    <div class="container">
    <form action="admin.php?action=<?php echo $results['formAction']?>" method="post">
          <input class="btn waves-effect waves-light red lighten-2" type="submit" formnovalidate name="cancel" value="Назад" />
        <?php if ( isset( $results['errorMessage'] ) ) { ?>
            <div class="errorMessage"><?php echo $results['errorMessage'] ?></div>
        <?php } ?>
        <ul>
            <li>
                <label for="type">Тип апартаменту</label>
                <input type="text" name="type" id="phone" placeholder="Тип апартаменту" required autofocus maxlength="255" value="<?php echo htmlspecialchars( $results['appartmentPrice']->type )?>" />
            </li>
            <li>
                <label for="square">Площа</label>
                <input name="square" id="address" placeholder="Площа" required maxlength="255" value="<?php echo htmlspecialchars( $results['appartmentPrice']->square )?>" >
            </li>
            <li>
                <label for="price_all">Ціна</label>
                <input name="price_all" id="email" placeholder="Ціна" required maxlength="255" value="<?php echo htmlspecialchars( $results['appartmentPrice']->price_all )?>">
            </li>
            <li>
                <label for="price_credit">Ціна в кредит</label>
                <input name="price_credit" id="email" placeholder="Ціна в кредит" required maxlength="255" value="<?php echo htmlspecialchars( $results['appartmentPrice']->price_credit )?>">
            </li>
            <li>
                <label for="conditions">Умови</label>
                <input name="conditions" id="email" placeholder="Умови" required maxlength="255" value="<?php echo htmlspecialchars( $results['appartmentPrice']->conditions )?>">
            </li>
        </ul>
        <div class="buttons">
            <input class="btn waves-effect" type="submit" name="saveChanges" value="Зберегти" />

        </div>
    </form>
     </div>
<?php include "views/include/footer.php" ?>