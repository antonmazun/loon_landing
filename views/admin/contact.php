<?php include "views/include/header.php" ?>
<div id="adminHeader">
    <p>You are logged in as <b><?php echo htmlspecialchars( $_SESSION['username']) ?></b>. <a href="admin.php?action=logout"?>Log out</a></p>
</div>

<h1 class="title-form"><?php echo $results['pageTitle']?></h1>
<div class="container">

<form action="admin.php?action=<?php echo $results['formAction']?>" method="post">
   <input class="btn waves-effect waves-light red lighten-2" type="submit" formnovalidate name="cancel" value="Назад" />
    <?php if ( isset( $results['errorMessage'] ) ) { ?>
        <div class="errorMessage"><?php echo $results['errorMessage'] ?></div>
    <?php } ?>
    <ul>
        <li>
            <label for="title">Номер телефону</label>
            <input type="text" name="phone" id="phone" placeholder="Name of the article" required autofocus maxlength="255" value="<?php echo htmlspecialchars( $results['contact']->phone )?>" />
        </li>
        <li>
            <label for="summary">Адреса</label>
            <input name="address" id="address" placeholder="Brief description of the article" required maxlength="255" value="<?php echo htmlspecialchars( $results['contact']->address )?>" >
        </li>
        <li>
            <label for="content">Email</label>
            <input name="email" id="email" placeholder="The HTML content of the article" required maxlength="255" value="<?php echo htmlspecialchars( $results['contact']->email )?>">
        </li>
    </ul>
    <div class="buttons">
        <input class="btn waves-effect" type="submit" name="saveChanges" value="Зберегти" />
    </div>
</form>
    </div>
<?php include "views/include/footer.php" ?>
