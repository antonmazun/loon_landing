<?php include "views/include/header.php" ?>
    <div id="adminHeader">
        <p>You are logged in as <b><?php echo htmlspecialchars( $_SESSION['username']) ?></b>. <a href="admin.php?action=logout"?>Log out</a></p>
    </div>
    <h1 class='title-form'><?php echo $results['pageTitle']?></h1>
    <div class="container">
    <form enctype="multipart/form-data" action="admin.php?action=<?php echo $results['formAction']?>" method="post">
         <input class="btn waves-effect waves-light red lighten-2" type="submit" formnovalidate name="cancel" value="Назад" />
        <?php if ( isset( $results['errorMessage'] ) ) { ?>
            <div class="errorMessage"><?php echo $results['errorMessage'] ?></div>
        <?php } ?>
        <ul class='form-wrap'>
            <li>
                <label for="layer">К-сть кімнат</label>
                <select name="room">
                    <option value="">--------------</option>
                    <option value="1">Одна</option>
                    <option value="2">Дві</option>
                    <option value="3">Три</option>
                </select>
            </li>
            <li>
                <label for="type">Тип квартири</label>
                <select name="type">
                    <option value="">--------------</option>
                    <option value="1">Тип 1</option>
                    <option value="2">Тип 2</option>
                    <option value="3">Тип 3</option>
                    <option value="4">Тип 4</option>
                    <option value="5">Тип 5</option>
                    <option value="6">Тип 6</option>
                    <option value="7">Тип 7</option>
                    <option value="8">Тип 8</option>
                    <option value="9">Тип 9</option>
                    <option value="10">Тип 10</option>
                </select>
            </li>
            <li>
                <label for="square">Площа квартири</label>
                <input type="text" name="square" id="square" placeholder="Площа"  />
            </li>
            <li>
                <label for="filename">Фото</label>
                <input type="file" name="filename" id="phone" placeholder="Фото"  />
            </li>
        </ul>
        <div class="buttons">
            <input class="btn waves-effect" type="submit" name="saveChanges" value="Зберегти" />

        </div>
    </form>
      </div>
<?php include "views/include/footer.php" ?>