DROP TABLE IF EXISTS `appartment`;
CREATE TABLE `appartment` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` varchar(150) CHARACTER SET utf8 NOT NULL,
  `square` varchar(30) CHARACTER SET utf8 NOT NULL,
  `price_all` varchar(100) CHARACTER SET utf8 NOT NULL,
  `price_credit` varchar(100) CHARACTER SET utf8 NOT NULL,
  `conditions` varchar(100) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (id)
);
INSERT INTO appartment (type,square, price_all,price_credit,conditions)
VALUES ('1-комнатная', 'від 27 до 45', 'від 486 000 грн','от 551 000 грн', 'перший внесок від 30%');

DROP TABLE IF EXISTS `static_page`;
CREATE TABLE `static_page` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` varchar(150) CHARACTER SET utf8 NOT NULL,
  `text` varchar(500) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (id)
);


INSERT INTO static_page (type, text)
VALUES ('Про нас', 'bla blka');

DROP TABLE IF EXISTS `contact`;
CREATE TABLE `contact` (
`id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `phone` varchar(55) NOT NULL,
  `address` varchar(150) CHARACTER SET utf8 NOT NULL,
  `email` varchar(500) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (id)
);

INSERT INTO contact (phone, address, email)
VALUES ('044 492 84 30', 'ул. Сормовскаяб 3', 'infometropoliya@m3kiev.ua');

DROP TABLE IF EXISTS `photo`;
CREATE TABLE `photo` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `room` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `square` varchar(255) NOT NULL,
  PRIMARY KEY (id)
);

INSERT INTO photo (room, type, filename,square)
VALUES ('1', '9', 'photos/257901141_61882.jpg', '54');