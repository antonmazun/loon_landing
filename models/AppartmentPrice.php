<?php
/**
 * Created by PhpStorm.
 * User: ivan
 * Date: 27.02.19
 * Time: 22:26
 */

namespace models;


use PDO;
use PDOException;

class AppartmentPrice
{
    public $id;
    public $type;
    public $square;
    public $price_all;
    public $price_credit;
    public $conditions;

    public function __construct( $data=array() ) {
        if ( isset( $data['id'] ) ) $this->id = (int) $data['id'];
        if ( isset( $data['type'] ) ) $this->type = $data['type'];
        if ( isset( $data['square'] ) ) $this->square = $data['square'];
        if ( isset( $data['price_all'] ) ) $this->price_all = $data['price_all'];
        if ( isset( $data['price_credit'] ) ) $this->price_credit = $data['price_credit'];
        if ( isset( $data['conditions'] ) ) $this->conditions = $data['conditions'];
    }

    public function storeFormValues ( $params ) {
        // Сохраняем все параметры
        $this->__construct( $params );
    }

    public static function getById( $id )
    {
        $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
        $sql = "SELECT * FROM `appartment` WHERE id = :id";
        $st = $conn->prepare( $sql );
        $st->bindValue( ":id", $id, PDO::PARAM_INT );
        $st->execute();
        $row = $st->fetch();
        $conn = null;
        if ( $row ) return new AppartmentPrice( $row );
    }

    public function insert() {
        if ( !is_null( $this->id ) ) trigger_error ( "ApartmentPrice::insert(): Attempt to insert an ApartmentPrice object that already has its ID property set (to $this->id).", E_USER_ERROR );
        try {
            $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
            $sql = "INSERT INTO appartment ( type, square, price_all, price_credit,conditions ) 
                            VALUES ( :type, :square, :price_all, :price_credit, :conditions )";
            $st = $conn->prepare ( $sql );
            $st->bindValue( ":type", $this->type, PDO::PARAM_STR );
            $st->bindValue( ":square", $this->square, PDO::PARAM_STR );
            $st->bindValue( ":price_all", $this->price_all, PDO::PARAM_STR );
            $st->bindValue( ":price_credit", $this->price_credit, PDO::PARAM_STR );
            $st->bindValue( ":conditions", $this->conditions, PDO::PARAM_STR );
            $st->execute();
            $this->id = $conn->lastInsertId();
            $conn = null;
        } catch (PDOException $e) {
            echo 'Не вдалось підключити: ' . $e->getMessage();
        }
    }

    public function update() {

        if ( is_null( $this->id ) ) trigger_error ( "ApartmentPrice::update(): Attempt to update an ApartmentPrice object that does not have its ID property set.", E_USER_ERROR );

        $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
        $sql = "UPDATE appartment SET type=:type, square=:square, price_all=:price_all, price_credit=:price_credit, conditions=:conditions WHERE id=:id";
        $st = $conn->prepare ( $sql );
        $st->bindValue( ":type", $this->type, PDO::PARAM_STR );
        $st->bindValue( ":square", $this->square, PDO::PARAM_STR );
        $st->bindValue( ":price_all", $this->price_all, PDO::PARAM_STR );
        $st->bindValue( ":price_credit", $this->price_credit, PDO::PARAM_STR );
        $st->bindValue( ":conditions", $this->conditions, PDO::PARAM_STR );
        $st->bindValue( ":id", $this->id, PDO::PARAM_INT );
        $st->execute();
        $conn = null;
    }

    public function delete() {
        if ( is_null( $this->id ) ) trigger_error ( "ApartmentPrice::delete(): Attempt to delete an ApartmentPrice object that does not have its ID property set.", E_USER_ERROR );
        $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
        $st = $conn->prepare ( "DELETE FROM appartment WHERE id = :id LIMIT 1" );
        $st->bindValue( ":id", $this->id, PDO::PARAM_INT );
        $st->execute();
        $conn = null;
    }

    public static function getList( $numRows=1000000 ) {
        $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
        $sql = "SELECT SQL_CALC_FOUND_ROWS * FROM appartment
                 LIMIT :numRows";

        $st = $conn->prepare( $sql );
        $st->bindValue( ":numRows", $numRows, PDO::PARAM_INT );
        $st->execute();
        $list = array();
        while ( $row = $st->fetch() ) {
          $appartment = new AppartmentPrice( $row );
          $list[] = $appartment;
        }
        // Получаем общее количество статей, которые соответствуют критерию
        $sql = "SELECT FOUND_ROWS() AS totalRows";
        $totalRows = $conn->query( $sql )->fetch();
        $conn = null;
        return ( array ( "results" => $list, "totalRows" => $totalRows[0] ) );
  }


}