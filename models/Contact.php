<?php

namespace models;


use PDO;
use PDOException;

class Contact
{
    public $id;
    public $phone;
    public $address;
    public $email;

    public function __construct( $data=array() ) {
        if ( isset( $data['id'] ) ) $this->id = (int) $data['id'];
        if ( isset( $data['phone'] ) ) $this->phone = $data['phone'];
        if ( isset( $data['address'] ) ) $this->address = $data['address'];
        if ( isset( $data['email'] ) ) $this->email = $data['email'];
    }

    public function storeFormValues ( $params ) {
    // Сохраняем все параметры
    $this->__construct( $params );
  }


    public static function getById( $id )
    {
        $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
        $sql = "SELECT * FROM `contact` WHERE id = :id";
        $st = $conn->prepare( $sql );
        $st->bindValue( ":id", $id, PDO::PARAM_INT );
        $st->execute();
        $row = $st->fetch();
        $conn = null;
        if ( $row ) return new Contact( $row );
    }
    public function insert() {
        if ( !is_null( $this->id ) ) trigger_error ( "Contact::insert(): Attempt to insert an Contact object that already has its ID property set (to $this->id).", E_USER_ERROR );
        try {
            $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
            $sql = "INSERT INTO contact ( phone, address, email ) VALUES ( :phone, :address, :email )";
            $st = $conn->prepare ( $sql );
            $st->bindValue( ":phone", $this->phone, PDO::PARAM_STR );
            $st->bindValue( ":address", $this->address, PDO::PARAM_STR );
            $st->bindValue( ":email", $this->email, PDO::PARAM_STR );
            $st->execute();
            $this->id = $conn->lastInsertId();
            $conn = null;
        } catch (PDOException $e) {
            echo 'Подключение не удалось: ' . $e->getMessage();
        }

    }

    public function update() {

        if ( is_null( $this->id ) ) trigger_error ( "Contact::update(): Attempt to update an Contact object that does not have its ID property set.", E_USER_ERROR );

        $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
        $sql = "UPDATE contact SET phone=:phone, address=:address, email=:email WHERE id=:id";
        $st = $conn->prepare ( $sql );
        $st->bindValue( ":phone", $this->phone, PDO::PARAM_STR );
        $st->bindValue( ":address", $this->address, PDO::PARAM_STR );
        $st->bindValue( ":email", $this->email, PDO::PARAM_STR );
        $st->bindValue( ":id", $this->id, PDO::PARAM_INT );
        $st->execute();
        $conn = null;
    }

    public function delete() {
        if ( is_null( $this->id ) ) trigger_error ( "Contact::delete(): Attempt to delete an Contact object that does not have its ID property set.", E_USER_ERROR );
        $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
        $st = $conn->prepare ( "DELETE FROM contact WHERE id = :id LIMIT 1" );
        $st->bindValue( ":id", $this->id, PDO::PARAM_INT );
        $st->execute();
        $conn = null;
    }
}