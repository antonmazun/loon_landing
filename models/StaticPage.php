<?php

namespace models;


use PDO;
use PDOException;

class StaticPage
{
    public $id;
    public $type;
    public $text;

    public function __construct( $data=array() ) {
        if ( isset( $data['id'] ) ) $this->id = (int) $data['id'];
        if ( isset( $data['type'] ) ) $this->type = $data['type'];
        if ( isset( $data['text'] ) ) $this->text = $data['text'];
    }

    public function storeFormValues ( $params ) {
        // Сохраняем все параметры
        $this->__construct( $params );
    }


    public static function getById( $id )
    {
//        var_dump('1'); die;
        $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
        $sql = "SELECT * FROM `static_page` WHERE id=:id";
        $st = $conn->prepare( $sql );
        $st->bindValue( ":id", $id, PDO::PARAM_INT );
        $st->execute();
        $row = $st->fetch();
        $conn = null;
        if ( $row ) return new StaticPage( $row );
    }

    public function insert() {
        if ( !is_null( $this->id ) ) trigger_error ( "StaticPage::insert(): Attempt to insert an StaticPage object that already has its ID property set (to $this->id).", E_USER_ERROR );
        try {
            $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
            $sql = "INSERT INTO static_page ( type, text) VALUES ( :type, :text)";
            $st = $conn->prepare ( $sql );
            $st->bindValue( ":type", $this->type, PDO::PARAM_STR );
            $st->bindValue( ":text", $this->text, PDO::PARAM_STR );
            $st->execute();
            $this->id = $conn->lastInsertId();
            $conn = null;
        } catch (PDOException $e) {
            echo 'Подключение не удалось: ' . $e->getMessage();
        }
    }

    public function update() {

        if ( is_null( $this->id ) ) trigger_error ( "StaticPage::update(): Attempt to update an StaticPage object that does not have its ID property set.", E_USER_ERROR );

        $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
        $sql = "UPDATE static_page SET type=:type, text=:text WHERE id=:id";
        $st = $conn->prepare ( $sql );
        $st->bindValue( ":type", $this->type, PDO::PARAM_STR );
        $st->bindValue( ":text", $this->text, PDO::PARAM_STR );
        $st->bindValue( ":id", $this->id, PDO::PARAM_INT );
        $st->execute();
        $conn = null;
    }

    public function delete() {
        if ( is_null( $this->id ) ) trigger_error ( "StaticPage::delete(): Attempt to delete an StaticPage object that does not have its ID property set.", E_USER_ERROR );
        $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
        $st = $conn->prepare ( "DELETE FROM static_page WHERE id = :id LIMIT 1" );
        $st->bindValue( ":id", $this->id, PDO::PARAM_INT );
        $st->execute();
        $conn = null;
    }
}