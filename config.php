<?php
ini_set( "display_errors", true );
date_default_timezone_set( "Europe/Kiev" );
define( "DB_DSN", "mysql:host=localhost;dbname=loon" );
define( "DB_USERNAME", "root" );
define( "DB_PASSWORD", "" );
define( "MODEL_PATH", "models" );
define( "TEMPLATE_PATH", "views" );
define( "ADMIN_USERNAME", "admin" );
define( "ADMIN_PASSWORD", "12345" );
require( MODEL_PATH . "/Contact.php" );
require( MODEL_PATH . "/StaticPage.php" );
require( MODEL_PATH . "/AppartmentPrice.php" );
require( MODEL_PATH . "/Photo.php" );

header('Content-Type: text/html; charset=utf-8');
function handleException( $exception ){
    echo "Sorry, a problem occurred. Please try later.";
    error_log( $exception->getMessage() );
}

set_exception_handler( 'handleException' );
