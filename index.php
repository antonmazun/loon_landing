<?php

use models\AppartmentPrice;
use models\Contact;
use models\Photo;
use models\StaticPage;

require( "config.php" );
$action = isset( $_GET['action'] ) ? $_GET['action'] : "";
switch ( $action ) {
//    case 'archive':
//        archive();
//        break;
    case 'viewArticle':
        viewArticle();
        break;
    default:
        homepage();
}

//function archive() {
//  $results = array();
//  $data = Article::getList();
//  $results['articles'] = $data['results'];
//  $results['totalRows'] = $data['totalRows'];
//  $results['pageTitle'] = "Article Archive | Widget News";
//  require( TEMPLATE_PATH . "/archive.php" );
//}

function viewArticle() {
  if ( !isset($_GET["contact"]) || !$_GET["contact"] ) {
    homepage();
    return;
  }
  $results = array();
  $results['article'] = Contact::getById( (int)$_GET["contact"] );
  $results['pageTitle'] = $results['contact']->title . " | Widget News";
  require( TEMPLATE_PATH . "/viewArticle.php" );
}

function homepage() {
  $results = array();
//  var_dump('main'); die;
  $contact = Contact::getById( 1 );
  $about_us = StaticPage::getById(1);
  $apartment_price = AppartmentPrice::getList();
  $photo = Photo::getListForSlider();
//  var_dump($data); die;
  $results['contact'] = $contact;
  $results['about-us'] = $about_us;
  $results['appartment-price'] = $apartment_price;
  $results['photo'] = $photo;
//    $results['totalRows'] = $data['totalRows'];
//  $results['pageTitle'] = "Widget News";
    require( TEMPLATE_PATH . "/client/main.php" );
}
