import $ from 'jquery';
import slick from 'slick-carousel';

class Root {
    constructor() {
        this.nav_item = document.querySelectorAll('.js-nav-item');
        this.close_btn = document.querySelectorAll('.close');
        this.count_room_btn = document.querySelectorAll('.js-count-room');
    }

    init() {
        let component = this;

        this.show_popup(this.nav_item);
        this.btn_close(this.close_btn);
        component.slider_hor();
        console.log(this.close_btn);
        // document.addEventListener('load', function () {
        //     component.slider_hor();
        //
        // });

        this.switched_count_room_flat(this.count_room_btn);

    }

    switched_count_room_flat(btns) {
        $(btns).click(function (e) {
            let data_count_room = $(this).data('count-room');
            console.log(data_count_room);
            let selector_for_slide_show = '.slider-wrapper-content[data-count="' + data_count_room + '"]';

            $('.slider-wrapper-content').addClass('hidden');
            $(selector_for_slide_show).removeClass('hidden');
            $(btns).removeClass('active-flat');
            $(this).addClass('active-flat');
            // $('.center-item').css({
            //     'position': 'absolute',
            //     'opacity': 0,
            //     'visibility' : "hidden"
            // }).addClass('slick-current');
            //  $('.center-item').first().addClass('slick-active slick-center');
            // $(selector_for_slide_show).css({
            //     'position': 'static',
            //     'opacity': 1,
            //     'visibility' : 'visible'
            // });
            // console.log("data_count_room" , data_count_room);
        })
    }

    slider_hor() {
        $('.room-1').slick({
            centerMode: true,
            centerPadding: '0',
            slidesToShow: 3,
            slidesToScroll: 1,
            prevArrow: $('.btn-prev.js-1'),
            nextArrow: $('.btn-next.js-1'),
            // responsive: [
            //     {
            //         breakpoint: 768,
            //         settings: {
            //             arrows: false,
            //             centerMode: true,
            //             centerPadding: '40px',
            //             slidesToShow: 3
            //         }
            //     },
            //     {
            //         breakpoint: 480,
            //         settings: {
            //             arrows: false,
            //             centerMode: true,
            //             centerPadding: '40px',
            //             slidesToShow: 1
            //         }
            //     }
            // ]
        });

        $('.room-2').slick({
            centerMode: true,
            centerPadding: '0',
            slidesToShow: 3,
            slidesToScroll: 1,
            prevArrow: $('.btn-prev.js-2'),
            nextArrow: $('.btn-next.js-2'),
            // responsive: [
            //     {
            //         breakpoint: 768,
            //         settings: {
            //             arrows: false,
            //             centerMode: true,
            //             centerPadding: '40px',
            //             slidesToShow: 3
            //         }
            //     },
            //     {
            //         breakpoint: 480,
            //         settings: {
            //             arrows: false,
            //             centerMode: true,
            //             centerPadding: '40px',
            //             slidesToShow: 1
            //         }
            //     }
            // ]
        });
    }


    show_popup(items) {
        const component = this;
        items.forEach((item) => {
            item.addEventListener('click', function (e) {
                e.preventDefault();
                items.forEach((inactive_tab) => {
                    inactive_tab.classList.remove('active-tab');
                });
                this.classList.add('active-tab');
                let data_attr = this.dataset.item;
                let popup_show_selector = '.popup-wrap[data-action=' + data_attr + ']';
                let popup = document.querySelector(popup_show_selector);
                if (!popup.classList.contains('visible')) {
                    component.hide_all_popup();
                    popup.classList.add('visible');
                }
            })
        })
    }

    hide_all_popup() {
        let all_popup = document.querySelectorAll('.popup-wrap');
        console.log('asdasdasdasd');
        all_popup.forEach((popup) => {
            if (popup.classList.contains('visible')) {
                popup.classList.remove('visible');
            }
        })
    }

    btn_close(btns) {
        const component = this;
        btns.forEach((btn) => {
            btn.addEventListener('click', function (e) {
                component.hide_all_popup();
                component.nav_item.forEach((inactive_tab) => {
                    inactive_tab.classList.remove('active-tab');
                })
            })
        })
    }
}

document.addEventListener('load', function () {

});

window.App = new Root().init();