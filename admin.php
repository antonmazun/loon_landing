<?php
use models\AppartmentPrice;
use models\Contact;
use models\Photo;
use models\StaticPage;

require( "config.php" );
session_start();
$action = isset( $_GET['action'] ) ? $_GET['action'] : "";
$username = isset( $_SESSION['username'] ) ? $_SESSION['username'] : "";

if ( $action != "login" && $action != "logout" && !$username ) {
      login();
      exit;
}
switch ( $action ) {
    case 'login':
        login();
        break;
    case 'logout':
        logout();
        break;
    case 'contact':
        editContact();
        break;
    case 'about-us':
        editStaticPage(1);
        break;
    case 'appartment-price':
        appartmentPriceList();
        break;
    case 'appartment-price-create':
            newAppartmentPrice();
        break;
    case 'appartment-price-update':
        $id = isset($_GET['id']) ? $_GET['id'] : 0;
        editAppartmentPrice($id);
        break;
    case 'appartment-price-delete':
        $id = isset($_GET['id']) ? $_GET['id'] : 0;
        deleteAppartmentPrice($id);
        break;
    case 'photo':
        photo();
        break;
    case 'photo-create':
        newPhoto();
        break;
    case 'photo-delete':
        $id = isset($_GET['id']) ? $_GET['id'] : 0;
        deletePhoto($id);
        break;
//    case 'editArticle':
//        editContact();
//        break;
//    case 'deleteContact':
//        deleteContact();
//        break;
    default:
        main();
}

function login() {
  $results = array();
  $results['pageTitle'] = "Admin Login | Widget News";
  if ( isset( $_POST['login'] ) ) {
    // Пользователь получает форму входа: попытка авторизировать пользователя
    if ( $_POST['username'] == ADMIN_USERNAME && $_POST['password'] == ADMIN_PASSWORD ) {
      // Вход прошел успешно: создаем сессию и перенаправляем на страницу администратора
      $_SESSION['username'] = ADMIN_USERNAME;
      header( "Location: admin.php" );

    } else {
      // Ошибка входа: выводим сообщение об ошибке для пользователя
      $results['errorMessage'] = "Incorrect username or password. Please try again.";
      require( TEMPLATE_PATH . "/admin/loginForm.php" );
    }
  } else {
    // Пользователь еще не получил форму: выводим форму
    require( TEMPLATE_PATH . "/admin/loginForm.php" );
  }

}

function logout() {
  unset( $_SESSION['username'] );
  header( "Location: admin.php" );
}

function main(){
    require( TEMPLATE_PATH . "/admin/main.php" );
}

function editContact() {
    $results = array();
    $results['pageTitle'] = "Редагування контактної інформації";
    $results['formAction'] = "contact";

    if ( isset( $_POST['saveChanges'] ) ) {
        // Пользователь получил форму редактирования статьи: сохраняем изменения
        if ( !$contact = Contact::getById( 1 ) ) {
            header( "Location: admin.php?error=contactNotFound" );
            return;
        }
        $contact->storeFormValues( $_POST );
        $contact->update();
        header( "Location: admin.php?status=changesSaved" );
    } elseif ( isset( $_POST['cancel'] ) ) {
        // Пользователь отказался от результатов редактирования: возвращаемся к списку статей
        header( "Location: admin.php" );
    } else {
        // Пользвоатель еще не получил форму редактирования: выводим форму
        $results['contact'] = Contact::getById( 1 );
        require( TEMPLATE_PATH . "/admin/contact.php" );
    }

}

function editStaticPage($id) {
    $results = array();
    $results['pageTitle'] = "Редагування сторінки \"Про нас\"";
    $results['formAction'] = "about-us";

    if ( isset( $_POST['saveChanges'] ) ) {
        // Пользователь получил форму редактирования статьи: сохраняем изменения
        if ( !$contact = StaticPage::getById( $id ) ) {
            header( "Location: admin.php?error=about-usNotFound" );
            return;
        }
        $contact->storeFormValues( $_POST );
        $contact->update();
        header( "Location: admin.php?status=changesSaved" );
    } elseif ( isset( $_POST['cancel'] ) ) {
        // Пользователь отказался от результатов редактирования: возвращаемся к списку статей
        header( "Location: admin.php" );
    } else {

        // Пользвоатель еще не получил форму редактирования: выводим форму
        $results['statpage'] = StaticPage::getById( $id );
        require( TEMPLATE_PATH . "/admin/static_page.php" );
    }

}

function appartmentPriceList(){
    $results = array();
    $data = AppartmentPrice::getList();
    $results['appartmentPrice'] = $data['results'];
    $results['totalRows'] = $data['totalRows'];
    $results['pageTitle'] = "Список апартаментів";

    if ( isset( $_GET['error'] ) ) {
        if ( $_GET['error'] == "articleNotFound" ) $results['errorMessage'] = "Error: Article not found.";
    }

    if ( isset( $_GET['status'] ) ) {
        if ( $_GET['status'] == "changesSaved" ) $results['statusMessage'] = "Your changes have been saved.";
        if ( $_GET['status'] == "articleDeleted" ) $results['statusMessage'] = "Article deleted.";
    }

    require( TEMPLATE_PATH . "/admin/appartment_price_list.php" );
}

function newAppartmentPrice() {
    $results = array();
    $results['pageTitle'] = "Новий апартамент";
    $results['formAction'] = "appartment-price-create";
    if ( isset( $_POST['saveChanges'] ) ) {
        // Пользователь получает форму редактирования статьи: сохраняем новую статью
        $article = new AppartmentPrice();
        $article->storeFormValues( $_POST );
        $article->insert();
        header( "Location: admin.php?action=appartment-price&status=changesSaved" );

    } elseif ( isset( $_POST['cancel'] ) ) {
        // Пользователь сбросил результаты редактирования: возвращаемся к списку статей
        header( "Location: admin.php?action=appartment-price" );
    } else {
        // Пользователь еще не получил форму редактирования: выводим форму
        $results['appartmentPrice'] = new AppartmentPrice();
        require( TEMPLATE_PATH . "/admin/appartment_price_form.php" );
    }

}

function editAppartmentPrice($id) {
    $results = array();
    $results['pageTitle'] = "Редагування апартаменту";
    $results['formAction'] = "appartment-price-update&id=".$id;

    if ( isset( $_POST['saveChanges'] ) ) {
        // Пользователь получил форму редактирования статьи: сохраняем изменения
        if ( !$contact = AppartmentPrice::getById( $id ) ) {
            header( "Location: admin.php?error=about-usNotFound" );
            return;
        }
        $contact->storeFormValues( $_POST );
        $contact->update();
        header( "Location: admin.php?action=appartment-price&status=changesSaved" );
    } elseif ( isset( $_POST['cancel'] ) ) {
        // Пользователь отказался от результатов редактирования: возвращаемся к списку статей
        header( "Location: admin.php?action=appartment-price" );
    } else {

        // Пользвоатель еще не получил форму редактирования: выводим форму
        $results['appartmentPrice'] = AppartmentPrice::getById( $id );
        require( TEMPLATE_PATH . "/admin/appartment_price_form.php" );
    }

}

function deleteAppartmentPrice($id) {
    if ( !$article = AppartmentPrice::getById( $id ) ) {
        header( "Location: admin.php?error=contactNotFound" );
        return;
    }
    $article->delete();
    header( "Location: admin.php?action=appartment-price&status=contactDeleted" );
}

function photo(){
    $results = array();
    $data = Photo::getList();
    $results['photo'] = $data['results'];
    $results['totalRows'] = $data['totalRows'];
    $results['pageTitle'] = "Список фотографій";

    if ( isset( $_GET['error'] ) ) {
        if ( $_GET['error'] == "articleNotFound" ) $results['errorMessage'] = "Error: Article not found.";
    }

    if ( isset( $_GET['status'] ) ) {
        if ( $_GET['status'] == "changesSaved" ) $results['statusMessage'] = "Your changes have been saved.";
        if ( $_GET['status'] == "articleDeleted" ) $results['statusMessage'] = "Article deleted.";
    }

    require( TEMPLATE_PATH . "/admin/photo_list.php" );
}

function deletePhoto($id) {
    if ( !$photo = Photo::getById( $id ) ) {
        header( "Location: admin.php?error=contactNotFound" );
        return;
    }
    $filename = $photo->filename;
    $photo->delete();
    $is_delete = $photo->checkPhoto($filename);
    if($is_delete){
        unlink($filename);
    }
//    var_dump($is_delete); die;
    header( "Location: admin.php?action=photo&status=contactDeleted" );
}

function newPhoto() {
    $results = array();
    $results['pageTitle'] = "Нове фото";
    $results['formAction'] = "photo-create";
    if ( isset( $_POST['saveChanges'] ) ) {
        // Пользователь получает форму редактирования статьи: сохраняем новую статью
        $article = new Photo();
        $uploadfile = 'photos/' . basename($_FILES['filename']['name']);
        if (move_uploaded_file($_FILES['filename']['tmp_name'], $uploadfile)) {
            $_POST['filename'] = $uploadfile;
        }
        $article->storeFormValues( $_POST );
        $article->insert();
        header( "Location: admin.php?action=photo&status=changesSaved" );

    } elseif ( isset( $_POST['cancel'] ) ) {
        // Пользователь сбросил результаты редактирования: возвращаемся к списку статей
        header( "Location: admin.php?action=photo" );
    } else {
        // Пользователь еще не получил форму редактирования: выводим форму
        $results['appartmentPrice'] = new Photo();
        require( TEMPLATE_PATH . "/admin/photo_form.php" );
    }

}




function newContact() {
  $results = array();
  $results['pageTitle'] = "New Contact";
  $results['formAction'] = "newArticle";
  if ( isset( $_POST['saveChanges'] ) ) {
    // Пользователь получает форму редактирования статьи: сохраняем новую статью
    $article = new \models\Contact();
    $article->storeFormValues( $_POST );
    $article->insert();
    header( "Location: admin.php?status=changesSaved" );

  } elseif ( isset( $_POST['cancel'] ) ) {
    // Пользователь сбросил результаты редактирования: возвращаемся к списку статей
    header( "Location: admin.php" );
  } else {
    // Пользователь еще не получил форму редактирования: выводим форму
    $results['article'] = new \models\Contact();
    require( TEMPLATE_PATH . "/admin/editContact.php" );
  }

}

function deleteContact() {
  if ( !$article = Contact::getById( (int)$_GET['contactId'] ) ) {
    header( "Location: admin.php?error=contactNotFound" );
    return;
  }
  $article->delete();
  header( "Location: admin.php?status=contactDeleted" );
}
function listArticles() {
  $results = array();
  $data = \models\Contact::getList();
  $results['articles'] = $data['results'];
  $results['totalRows'] = $data['totalRows'];
  $results['pageTitle'] = "All Articles";

  if ( isset( $_GET['error'] ) ) {
    if ( $_GET['error'] == "articleNotFound" ) $results['errorMessage'] = "Error: Article not found.";
  }

  if ( isset( $_GET['status'] ) ) {
    if ( $_GET['status'] == "changesSaved" ) $results['statusMessage'] = "Your changes have been saved.";
    if ( $_GET['status'] == "articleDeleted" ) $results['statusMessage'] = "Article deleted.";
  }

  require( TEMPLATE_PATH . "/admin/listArticles.php" );
}
